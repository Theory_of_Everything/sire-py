# Aisaka Bot

Aisaka bot is a discord bot oriented to function as a utility bot for Programmers.   

For Now this bot doesn't do much, but it will eventually do more (a lot more).  

The Bot's [Trello Board](https://trello.com/b/XWWN39gT/aisaka-bot)

## Building/Running the bot
To run Aisaka you will need to install the dependencies  
`$ pip install -U discord.py`  
`$ pip install -U dotenv`  

Create a .env file for bot settings:    
```
#.env  
DISCORD_TOKEN=<yourtoken> # <--- Replace <your_token> wth your bot's Token (omit quotes)
```
